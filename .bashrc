export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\W\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
export PATH="$PATH"
export GOPATH="$HOME/projects/go"
export JAVA_HOME="$HOME/.sdkman/candidates/java/current"
export SCALA_HOME="/opt/scala-2.11.8"
export POSTGRES_HOME="/Applications/Postgres.app/Contents/Versions/9.5"
export PATH="/usr/local/bin:$PATH:$SCALA_HOME/bin:$POSTGRES_HOME/bin"
export EDITOR=nvim
export NODE_ENV="dev"
export TERM="xterm-256color"
export FZF_DEFAULT_COMMAND='rg --files --hidden --follow'
export REGISTRY=docker-registry.hq.local
export RIPGREP_CONFIG_PATH=$HOME/.rgrc
alias ls='ls -GFh --color'
alias l='ls '
alias ll='ls -GFhal'
alias vim=nvim
alias vi=nvim
alias nv=nvim
alias nvc='nvim ~/.config/nvim/init.vim'
alias tl='task long'
alias tn='task next'
alias ts='task summary'

alias sudo="sudo -E "
alias TODO="$EDITOR ~/TODO"
alias scripts="cat package.json | jq .scripts"
alias xclip="xclip -selection clipboard"
alias gst="git status"
alias gs="git status"
alias gco="git checkout"
alias gd="git diff"
alias gc="git commit -m"
alias ga="git add"
alias nvc="nvim ~/.config/nvim/init.vim"
alias nvb="nvim ~/.bashrc"
alias sudo="sudo -E "

export HISTCONTROL=ignoredups:erasedups:ignorespace   # no duplicate entries
export HISTSIZE=100000                                # big big history
export HISTFILESIZE=100000                            # big big history
shopt -s histappend                                   # append to history, don't overwrite it

# Save and reload the history after each command finishes
export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

# Put this in a function one day
#man -P cat mkdir | grep "   \-p" -A 2


#git() {
#  PARAMS=""
#  for PARAM in "$@"
#  do
#    PARAMS="${PARAMS} '${PARAM}'"
#  done
#
#  if [ "$1" == "commit" ]; then
#    command /bin/bash -c git $PARAMS --no-verify
#  elif [ "$1" == "push" ]; then
#    command /bin/bash -c git $PARAMS --no-verify
#  else
#    command /bin/bash -c git $PARAMS
#  fi
#}

printbat() {
  echo ''
  echo '         /(_M_)\ '
  echo '        |       |'
  echo '         \/~V~\/ '
  echo ''
  echo ''
}

fwatch() {
  FILE=$1
  COMMAND="${@:2}"
  while inotifywait $FILE
    do $COMMAND
  done
}

vers() {
  PACKAGE=$1
  PROJECT_DIR=$(git rev-parse --show-toplevel)
  cat $PROJECT_DIR/node_modules/$PACKAGE/package.json | jq .version
}

if type -P nvim > /dev/null; then
  alias vim="nvim"
  alias nv="nvim"
fi

currenttasks() {
  if type -P task > /dev/null; then
    if type -P jq > /dev/null; then
      # Show my tasks!
      bold=$(tput bold)
      normal=$(tput sgr0)
      echo -e "${bold}NOW:${normal}  \e[33m$(task +ACTIVE export | jq '.[] | "\(.description) [id: \(.id)]"' -r)\e[0m"
      echo -e "${bold}NEXT:${normal} \e[33m$(task +READY -ACTIVE export | jq 'sort_by(-.urgency) | .[0] | "\(.description) [id: \(.id)]"' -r)\e[0m"
      echo "$(task +PENDING count) pending. $(task -COMPLETED due:today count) Due today"
    else
      echo Could not find jq
    fi
  else
    echo Could not find taskwarrior
  fi
}

focus() {
  clear
  printbat
  currenttasks
}

focus

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/john/.sdkman"
[[ -s "/home/john/.sdkman/bin/sdkman-init.sh" ]] && source "/home/john/.sdkman/bin/sdkman-init.sh"
